using GameManagement;
using UnityEngine;

namespace SpaceShip
{
    public class PlayerAttribute : MonoBehaviour, IDamageable
    {

        [SerializeField] private float hp;
        [SerializeField] private SoundManager soundManager;
        [SerializeField] private GameManager gameManager;

        private void Start()
        {
            soundManager = FindObjectOfType<SoundManager>();
            gameManager = FindObjectOfType<GameManager>();
        }

        public void TakeHit(float damage)
        {
            hp -= damage;

            Debug.Log("Taken!!");
            
            if (hp <=0)
            {
                soundManager.PlaySoundExplosion();
                gameManager.EndGame();
                Debug.Log("PlayerDie");
                Destroy(gameObject);
            }
        }
    }
}
