﻿using GameManagement;
using UnityEngine;

namespace SpaceShip
{
    public class EnemyController : MonoBehaviour, IDamageable
    {
        [SerializeField] protected Transform playerTransform;
        [SerializeField] protected float enemyShipSpeed = 5;
        public bool isDead;
        [SerializeField] protected SpriteRenderer enemyRenderer;

        [SerializeField] protected SpriteRenderer playerRenderer;
        [SerializeField] protected ScoreManager scoreManager;
        [SerializeField] protected SoundManager soundManager;

        private float chasingThresholdDistance = 1.0f;
        
        [SerializeField] protected GameObject enemyShip;

        [SerializeField] protected float hp;
        
        [SerializeField] protected float countdownTime;
        
        [SerializeField] protected float countdownTimer;
        
        [SerializeField] protected Color takeDamColor;

        [SerializeField] protected Color normalColor;

        [SerializeField] protected GameObject enemyBullet;

        [SerializeField] protected float cooldownFire;
        
        [SerializeField] protected float cooldownFireMax;

        [SerializeField] protected Vector2 gunPosition;

        [SerializeField] protected Vector2 enemyPosition;


        protected void Start()
        {
            Debug.Log("Spawn!");
            scoreManager = FindObjectOfType<ScoreManager>();
            soundManager = FindObjectOfType<SoundManager>();

        }

        private void OnDrawGizmos()
        {
            CollisionDebug();
        }

        void CollisionDebug()
        {
            if (enemyRenderer != null && playerRenderer != null)
            {
                if (intersectAABB(enemyRenderer.bounds,playerRenderer.bounds))
                {
                    Gizmos.color = Color.red;
                }
                else
                {
                    Gizmos.color = Color.white;

                }
                Gizmos.DrawCube(enemyRenderer.bounds.center, 2 * enemyRenderer.bounds.extents);
                Gizmos.DrawCube(playerRenderer.bounds.center, 2 * playerRenderer.bounds.extents);

            }
            
        }
        
        bool intersectAABB(Bounds a,Bounds b)
        {
            return (a.min.x <= b.max.x && a.max.x >= b.min.x) && 
                   (a.min.y <= b.max.y && a.max.y >= b.min.y);
        }
        void Update()
        {
            //MoveToPlayer();
            countdownTimer -= Time.deltaTime;
            ColorCooldownTime();
            Fire();

            GunPositionUpdate();
                
            if (cooldownFire <= cooldownFireMax)
            {
                cooldownFire -= Time.deltaTime;
            }
        }
        
        
        public void GunPositionUpdate()
        {
            
            enemyPosition = transform.position;

            gunPosition = enemyPosition + new Vector2();
            gunPosition.y = enemyPosition.y - 0.5f;
        }
        
        private void MoveToPlayer()
        {
            Vector3 enemyPosition = transform.position;
            Vector3 playerPosition = playerTransform.position;
            enemyPosition.z = playerPosition.z; // ensure there is no 3D rotation by aligning Z position
            
            Vector3 vectorToTarget = playerPosition - enemyPosition; // vector from this object towards the target location
            Vector3 directionToTarget = vectorToTarget.normalized;
            Vector3 velocity = directionToTarget * enemyShipSpeed;
                       
            float distanceToTarget = vectorToTarget.magnitude;

            if (distanceToTarget > chasingThresholdDistance)
            {
                transform.Translate(velocity * Time.deltaTime);
            }

        }

        virtual public void TakeHit(float damage)
        {
            hp -= damage;

            countdownTimer = countdownTime;
            
            enemyRenderer.color = takeDamColor;

            if (hp <=0)
            {
                soundManager.PlaySoundExplosion();
                Debug.Log("Dieeeeeee");
                scoreManager.GetScore(1);
                Dead();
            }

        }
        
        
        void ColorCooldownTime()
        {
            if (countdownTimer <= 0)
            {
                enemyRenderer.color = normalColor;
            }
        }
        
        protected void Dead()
        {
            isDead = true;
            Destroy(enemyShip);
        }

        private void Fire()
        {
            if (cooldownFire <= 0)
            {
                Debug.Log("EnemyFire!!");
                Instantiate(enemyBullet,gunPosition,Quaternion.identity);
                cooldownFire = cooldownFireMax;
            }
        }

    }    
}

