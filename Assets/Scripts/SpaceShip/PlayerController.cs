﻿using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;


namespace SpaceShip
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float playerShipSpeed = 10;
        
        private Vector2 movementInput = Vector2.zero;
        [SerializeField] private Vector2 playerPosition;
        [SerializeField] private Vector2 gunPosition;
        
        [SerializeField] private GameObject bullet;


        private float xMin;
        private float xMax;
        private float yMin;
        private float yMax;

        private float padding = 0.5f;
        private void Start()
        {
            SetupMoveBoundnaries();
        }
        
        private void SetupMoveBoundnaries()
        {
            Camera gameCamera = Camera.main;
            
            xMin = gameCamera.ViewportToWorldPoint(new Vector2(0, 0)).x + padding;
            xMax = gameCamera.ViewportToWorldPoint(new Vector2(1, 0)).x - padding;

            yMin = gameCamera.ViewportToWorldPoint(new Vector2(0, 0)).y + padding ;
            yMax = gameCamera.ViewportToWorldPoint(new Vector2(0, 1)).y - padding;
        }
        private void Update()
        {
            Move();
            Fire();
            GunPositionUpdate();
        }
        
        private void Move()
        {
            var inputDirection = movementInput.normalized;
            var inPutVelocity = inputDirection * playerShipSpeed;

            var gravity = new Vector2(0, -2);
            var force = new Vector2(2, 0);

            var finalVelo = inPutVelocity + gravity + force;

            Debug.DrawRay(transform.position,inPutVelocity,Color.green);
            Debug.DrawRay(transform.position,gravity,Color.red);
            Debug.DrawRay(transform.position,inPutVelocity,Color.blue);
            Debug.DrawRay(transform.position,force,Color.yellow);

            var newXPos = transform.position.x + inPutVelocity.x * Time.deltaTime;
            var newYPos = transform.position.y + inPutVelocity.y * Time.deltaTime;
            
            newXPos = Mathf.Clamp(newXPos, xMin, xMax);
            newYPos = Mathf.Clamp(newYPos, yMin, yMax);

            transform.position = new Vector2(newXPos, newYPos);
        }
        
        private void Fire()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("Fire!!");
                Instantiate(bullet,gunPosition,quaternion.identity);
            }
        }

        public void GunPositionUpdate()
        {
            
            playerPosition = transform.position;
            
            gunPosition = playerPosition + new Vector2();
            gunPosition.y = playerPosition.y + 0.5f;
        }
        
        
        public void OnMove(InputAction.CallbackContext context)
        {
            movementInput = context.ReadValue<Vector2>();
        }
    }
}
