

namespace SpaceShip
{
    public interface IDamageable
    {
        void TakeHit(float damage);

    }
}
