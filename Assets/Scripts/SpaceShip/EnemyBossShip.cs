using UnityEngine;

namespace SpaceShip
{
    public class EnemyBossShip : EnemyController
    {
        

        override public void TakeHit(float damage)
        {
            hp -= damage;

            countdownTimer = countdownTime;

            enemyRenderer.color = takeDamColor;

            if (hp <= 0)
            {
                soundManager.PlaySoundExplosion();
                Debug.Log("Boss Die");
                scoreManager.GetScore(3);
                Dead();
            }
        }

    }
}
