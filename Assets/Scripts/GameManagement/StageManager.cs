using UnityEngine;
using UnityEngine.SceneManagement;
using Utility;

namespace GameManagement
{
    public class StageManager : MonoSingleton<StageManager>
    {
        [SerializeField] private int nextStageNumber;
        [SerializeField] private int maxStageNumber;
        [SerializeField] private GameManager gameManager;


        
        private void Start()
        {
            gameManager = FindObjectOfType<GameManager>();
            nextStageNumber = 0;
        }

        public void LoadStage0()
        {
            SceneManager.LoadScene(0);
        }
        
        public void LoadStage1()
        {
            SceneManager.LoadScene(1);
        }
        
        public void NextStage()
        {
            if (nextStageNumber <= maxStageNumber)
            {
                ++nextStageNumber;
            }
            if (nextStageNumber > maxStageNumber)
            {
                nextStageNumber = 0;
            }
            gameManager.SetDefaultStartUi();
            gameManager.gameOverUi.SetActive(false);
            ScoreManager.Instance.ResetScore();
            SceneManager.LoadScene(nextStageNumber);
        }
        
    }
}
