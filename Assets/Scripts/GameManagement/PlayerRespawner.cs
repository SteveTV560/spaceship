using Unity.Mathematics;
using UnityEngine;


namespace GameManagement

{
    public class PlayerRespawner : MonoBehaviour
    {
        [SerializeField] private GameObject objectSpawn;
        
        [SerializeField] private Vector2 spawnPoint;
        
        void Start()
        {
            spawnPoint = transform.position;
        }
        
        void Update()
        {
            spawnPoint = transform.position;
        }

        public void Spawn()
        {
            Instantiate(objectSpawn, spawnPoint, quaternion.identity);
        }
    }
}
