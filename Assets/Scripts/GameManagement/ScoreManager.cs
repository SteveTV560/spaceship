using UnityEngine;
using UnityEngine.UI;
using Utility;

namespace GameManagement
{
    public class ScoreManager : MonoSingleton<ScoreManager>
    {
        [SerializeField] private int score;

        [SerializeField] private int scoreWinPoint;

        [SerializeField] private Text scoreText;

        [SerializeField] private GameManager gameManager;
        
        private void Start()
        {
            gameManager = FindObjectOfType<GameManager>();
            score = 0;
            scoreText.text = $"Score : {score.ToString()}";
        }
        
        private void Update()
        {
            if (score >= scoreWinPoint)
            {
                gameManager.EndGame();
            }
            
        }
        
        public void ResetScore()
        {
            score = 0;
            scoreText.text = $"Score : {score.ToString()}";
            Debug.Log($"Score Reset!!");
        }
        
        public void GetScore(int scorePlus)
        {
            score += scorePlus;
            scoreText.text = $"Score : {score.ToString()}";
            Debug.Log($"Score : {score}");
        }
    }
}
