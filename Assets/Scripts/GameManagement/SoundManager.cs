using UnityEngine;
using Utility;

namespace GameManagement
{
    public class SoundManager : MonoSingleton<SoundManager>
    {

        [SerializeField] private AudioClip explosion;
        [SerializeField] private AudioClip fire;
        [SerializeField] private AudioSource audioSource;
        

        private void Start()
        {
            GetComponent<AudioSource>();
        }

        public void PlaySound(AudioClip audioClip, AudioSource audioSource)
        {
            audioSource.clip = audioClip;
            audioSource.Play();
        }

        public void PlaySoundExplosion()
        {
            PlaySound(explosion, audioSource);
        }

        public void PlaySoundFire()
        {
            PlaySound(fire, audioSource);
        }
    }
}
