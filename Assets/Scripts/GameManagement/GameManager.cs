using UnityEngine;
using Utility;

namespace GameManagement

{
    public class GameManager : MonoSingleton<GameManager>
    {

        [SerializeField] public GameObject startUi;
        
        [SerializeField] public GameObject gameOverUi;
        
        [SerializeField] private ScoreManager scoreManager;

        [SerializeField] private StageManager stageManager;

        public void SetDefaultStartUi()
        {
            gameOverUi.SetActive(false);
            startUi.SetActive(true);
        }

        void Start()
        {
            gameOverUi.SetActive(false);
            stageManager = FindObjectOfType<StageManager>();
            scoreManager = FindObjectOfType<ScoreManager>();
        }
        
        public void PlayGame()
        {
            startUi.SetActive(false);
            
            FindObjectOfType<PlayerRespawner>().Spawn();
            
            FindObjectOfType<EnermySpawner>().Spawn();
            
            GameObject.FindGameObjectsWithTag("EnemySpawner");

        }

        public void EndGame()
        {
            gameOverUi.SetActive(true);
            Debug.Log("Win");
            
        }

        public void Quit()
        {
            Application.Quit();
        }

        public void LoadNextStage()
        {
            //StageManager.Instance.NextStage();
            stageManager.NextStage();
        }
        
        public void RestartGame()
        {
            scoreManager.ResetScore();
            gameOverUi.SetActive(false);
            Destroy(GameObject.FindGameObjectWithTag("Player"));
            Destroy(GameObject.FindGameObjectWithTag("Enermy"));
            Destroy(GameObject.FindGameObjectWithTag("Bullet"));
            FindObjectOfType<PlayerRespawner>().Spawn();
            FindObjectOfType<EnermySpawner>().Spawn();
        }
    }
}
