using UnityEngine;

namespace GameManagement
{
    public class EnermySpawner : MonoBehaviour
    {
        [SerializeField]
        private float spawnTime;
        
        private float spawnTimeCountDown;

        private bool spawnable;
        
        [SerializeField]
        private GameObject objectSpawn;
        
        [SerializeField]
        private Vector2 spawnPoint;
        
        void Start()
        {
            DisableSpawn();
            spawnPoint = transform.position;
            
        }
        
        void Update()
        {
            spawnPoint = transform.position;
            RespawnCountDown();
        }
        
        void RespawnCountDown()
        {
            if (spawnTimeCountDown >0)
            {
                spawnTimeCountDown -= Time.deltaTime;
            }
            else if (spawnTimeCountDown <=0 && spawnable)
            {
                Spawn();
                spawnTimeCountDown = spawnTime;
            }
        }

        public void Spawn()
        {
            Instantiate(objectSpawn, spawnPoint,Quaternion.identity);
        }
        
        public void EnebleSpawn()
        {
            spawnable = true;
        }
        
        public void DisableSpawn()
        {
            spawnable = false;
        }
    }
}
