using System.Collections;
using System.Collections.Generic;
using Bullet;
using UnityEngine;

namespace Bullet
{
    public class EnemyMissileBullet : MissileBullet
    {

        [SerializeField] private Vector3 playerPosition;


        protected override void Start()
        {
            firesSource.clip = fireClip;
            firesSource.Play();
            playerPosition = GameObject.FindWithTag("Player").transform.position;
        }

        protected override void OnMove()
        {
            if (playerPosition != new Vector3(0, 0, 0))
            {
                Vector3 playerDir = playerPosition.normalized;
                transform.Translate(playerDir * bulletSpeed * Time.deltaTime);
            }
            else
            {
                transform.Translate(new Vector3(0, -bulletSpeed, 0).normalized * bulletSpeed * Time.deltaTime);
            }
        }
    }
}
