using System.Collections;
using System.Collections.Generic;
using Bullet;
using UnityEngine;

namespace Bullet
{
    public class MissileBullet : NormalBullet
    {

        [SerializeField] protected Vector3 enemyPosition;
        protected override void Start()
        {
            firesSource.clip = fireClip;
            firesSource.Play();
            enemyPosition = GameObject.FindWithTag("Enermy").transform.position;
        }

        protected override void OnMove()
        {
            if (enemyPosition != new Vector3(0,0,0))
            {
                Vector3 enemyDir = enemyPosition.normalized; 
                transform.Translate(enemyDir * bulletSpeed * Time.deltaTime);
            }
            else
            {
                transform.Translate(new Vector3(0, -bulletSpeed, 0).normalized * bulletSpeed * Time.deltaTime);
            }

        }
    }
}

