using UnityEngine;

namespace Bullet

{
    public class NormalBullet : MonoBehaviour
    {
        [SerializeField] protected GameObject self;
        [SerializeField] protected float bulletSpeed;
        [SerializeField] public float damage;
        [SerializeField] public float timeCounter;
        
        [SerializeField] protected AudioClip fireClip;
        [SerializeField] protected AudioSource firesSource;
        protected virtual void  Start()
        {
            self = gameObject;
            firesSource.clip = fireClip;
            firesSource.Play();
        }

        protected virtual void Update()
        {
            OnMove();
            TimeCount();
        }

        protected virtual void OnMove()
        {
            float bulletMoveY = transform.position.y + bulletSpeed * Time.deltaTime;
            float bulletMoveX = transform.position.x;
            transform.position = new Vector2(bulletMoveX, bulletMoveY);
        }
        
        protected void OnCollisionEnter2D(Collision2D other)
        {
            var target = other.gameObject.GetComponent<SpaceShip.IDamageable>();
            target?.TakeHit(damage);
            Destroy(gameObject);
        }
        
        
        protected void TimeCount()
        {
            timeCounter -= Time.deltaTime;
            if (timeCounter <= 0)
            {
                Destroy(self);
            }
        }
        
    }
}
